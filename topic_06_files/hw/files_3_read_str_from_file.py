"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""
from files_1_save_dict_to_file_classic import save_dict_to_file_classic

# pickle
import pickle
import os


def read_str_from_file_pickle(path: str):
    if os.path.exists(path):
        with open(path, 'rb') as file:
            data_from_file = pickle.load(file)
        print(data_from_file)
    else:
        print('file doesn\'t exist')


# build-in
def read_str_from_file(path: str):
    if os.path.exists(path):
        with open(path, 'r') as file:
            data_from_file = file.read()
        print(data_from_file)
    else:
        print('file doesn\'t exist')


if __name__ == '__main__':
    txt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse blandit lorem sed mollis malesuada. Duis dignissim efficitur ullamcorper. Aenean sit amet aliquet turpis, ut sagittis nibh. Phasellus luctus odio purus, ut viverra sem tristique ac. Donec aliquet ipsum vitae malesuada faucibus. Duis sollicitudin placerat lacinia. Vestibulum eget massa aliquet justo convallis volutpat at cursus urna. Fusce ullamcorper tempus ipsum. Proin blandit, massa nec pulvinar ultrices, mi felis gravida est, consectetur euismod mauris ex eget ligula. Curabitur eget ante augue. Nam sit amet velit neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nam id vehicula ipsum, non faucibus elit. Quisque metus augue, luctus nec consectetur at, suscipit tincidunt eros. Curabitur sagittis metus at maximus convallis. Phasellus nec erat dolor. \n Mauris dignissim est dui, vel gravida orci dapibus quis. Phasellus bibendum odio ut purus malesuada varius. Donec dictum felis ultricies nisl scelerisque vestibulum. Ut ornare, odio.'
    save_dict_to_file_classic('text.txt', txt)

    read_str_from_file('text.txt')
    read_str_from_file('aaa.txt')
