"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""
import pickle
from files_3_read_str_from_file import read_str_from_file_pickle


def save_dict_to_file_pickle(path: str, my_dict: dict):
    with open(path, 'wb') as file:
        pickle.dump(my_dict, file)


if __name__ == '__main__':
    data = {
        'a': [1, 2.0, 3, 4 + 6j],
        'b': ("character string", b"byte string"),
        'c': {None, True, False},
        'banana': 'банан',
        'apple': 'яблоко',
        'pear': 'груша'
    }
    pathname = 'test.pkl'
    save_dict_to_file_pickle(pathname, data)
    read_str_from_file_pickle(pathname)
