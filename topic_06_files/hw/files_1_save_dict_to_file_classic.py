"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path: str, my_dict: dict):
    with open(path, 'w') as file:
        file.write(str(my_dict))


if __name__ == '__main__':
    test_dict = {
        'banana': 'банан',
        'apple': 'яблоко',
        'pear': 'груша'
    }
    save_dict_to_file_classic('test.txt', test_dict)
