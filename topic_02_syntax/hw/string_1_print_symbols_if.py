"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(user_string):
    user_string_len = len(user_string)
    if user_string_len < 1:
        print("Empty string!")
    elif user_string_len > 5:
        print(user_string[:3:] + user_string[-3::])
    else:
        print(user_string[0] * user_string_len)
