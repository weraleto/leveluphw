"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    total = 0
    for n in range(1, 112 + 3, 3):
        total += n
    return total
