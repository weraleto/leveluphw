"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""


def arithmetic(a, b, op):
    """
    simple calculator function
    """
    operators = {
        '/': lambda x, y: x / y,
        '*': lambda x, y: x * y,
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y
    }
    if op == '/' and b == 0:
        return 'division by zero'
    if op in operators:
        return operators[op](a, b)
    else:
        return 'Unknown operator'
