"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(num):
    if type(num) != int:
        return "Must be int!"
    elif num <= 0:
        return "Must be > 0!"
    else:
        count = 0
        for digit in str(num):
            if int(digit) % 2 != 0:
                count += 1
        return count
