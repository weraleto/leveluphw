"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str1, str2):
    str1_len, str2_len = len(str1), len(str2)
    if str1 != str2:
        check1 = str1_len == 0 or str2_len == 0
        check2 = str1_len > str2_len and str1.find(str2) > -1 or str1_len < str2_len and str2.find(str1) > -1
        return check1 or check2

    return False
