"""
lambda суммирует аргументы a, b и c и выводит результат
"""

x = (lambda a, b, c: sum((a, b, c)))(18, 83, 54)

if __name__ == '__main__':
    print(x)
