"""
lambda перемножает аргументы a, b и c и выводит результат
"""

x = (lambda a, b, c: a * b * c)(5, 3, 4)

if __name__ == '__main__':
    print(x)
