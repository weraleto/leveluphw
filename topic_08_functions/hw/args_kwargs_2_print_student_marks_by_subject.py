"""
Функция print_student_marks_by_subject.

Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
"""


def print_student_marks_by_subject(name, **mark_list):
    print(name)
    for subj, marks in mark_list.items():
        subj_mark = ', '.join(str(i) for i in marks) if type(marks) in (tuple, list) else marks
        print(subj, subj_mark, sep=': ', end="; ")


if __name__ == '__main__':
    print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5))
