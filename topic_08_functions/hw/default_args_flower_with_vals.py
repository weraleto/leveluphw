"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower="ромашка", color="белый", price=10.25):
    if type(flower) is not str or type(color) is not str:
        print('Flower and color must be string!')
    elif not (isinstance(price, int) or isinstance(price, float)):
        print('Price must be a number float or int!')
    elif not (0 < price < 10000):
        print('Price must be more than 0 and less than 10 000!')
    else:
        print(f"Цветок: {flower} | Цвет: {color} | Цена: {price}")


if __name__ == '__main__':
    flower_with_default_vals(price='15')
    flower_with_default_vals(price=100500)
    flower_with_default_vals(flower=212)
    flower_with_default_vals(color=7777)

    flower_with_default_vals(flower="Тюльпан")
    flower_with_default_vals(color="белый")
    flower_with_default_vals(price=15)

    flower_with_default_vals(flower='Пион', color='розовый')
    flower_with_default_vals(price=200, flower='Пион')
    flower_with_default_vals(price=200, color='голубой')

    flower_with_default_vals(price=156, color='желтый', flower='Орхидея')


