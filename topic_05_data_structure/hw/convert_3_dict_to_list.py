"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(d1: dict):
    if type(d1) is not dict:
        return 'Must be dict!'
    elif not d1:
        return [], [], 0, 0
    keys, values = d1.keys(), d1.values()
    return (
        list(keys),
        list(values),
        len(set(keys)),
        len(set(values))
    )
