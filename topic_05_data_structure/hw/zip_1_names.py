"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(var1: list, var2: set):
    if type(var1) is not list:
        return 'First arg must be list!'
    elif type(var2) is not set:
        return 'Second arg must be set!'
    elif not len(var1):
        return 'Empty list!'
    elif not len(var2):
        return 'Empty set!'
    return list(zip(var1, var2))


if __name__ == '__main__':
    l2 = {'b', 'c'}
    print(zip_names(['a'], {'b', 'c'}))
