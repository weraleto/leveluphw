"""
Функция zip_colour_shape.

Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.

Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_colour_shape(var1: list, var2: tuple):
    if type(var1) is not list:
        return 'First arg must be list!'
    elif type(var2) is not tuple:
        return 'Second arg must be tuple!'
    elif not len(var1):
        return 'Empty list!'
    elif not len(var2):
        return 'Empty tuple!'
    return list(zip(var1, var2))
