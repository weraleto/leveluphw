from topic_05_data_structure.hw.dict_1_get_translation_by_word import get_translation_by_word


def test_get_translation_by_word_wrong_types():
    assert get_translation_by_word([1, 2, 3], 'ru') == "Dictionary must be dict!"
    assert get_translation_by_word('eng', 'ru') == "Dictionary must be dict!"
    assert get_translation_by_word({'ru': ['asd', 'ldo', 'eee']}, 2) == "Word must be str!"
    assert get_translation_by_word({'ru': ['asd', 'ldo', 'eee']}, ['aa', 'bb']) == "Word must be str!"


def test_get_translation_by_word_empty_params():
    assert get_translation_by_word({}, 'test') == "Dictionary is empty!"
    assert get_translation_by_word(dict(), 'test') == "Dictionary is empty!"
    assert get_translation_by_word({'ru': ['asd', 'ldo', 'eee']}, '') == "Word is empty!"


def test_get_translation_by_word_ok():
    assert get_translation_by_word({'apple': ['яблоко'], 'pear': ['груша']}, 'apple')
    assert get_translation_by_word({'enormous': ['очень большой', 'огромный'], 'inspiration': ['вдохновение']},
                                   'enormous') == ['очень большой', 'огромный']
    assert get_translation_by_word({'apple': ['яблоко'], 'pear': ['груша']},
                                   'banana') == "Can't find Russian word: banana"
