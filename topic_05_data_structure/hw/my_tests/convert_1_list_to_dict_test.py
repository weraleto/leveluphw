from topic_05_data_structure.hw.convert_1_list_to_dict import list_to_dict


def test_list_to_dict_wrong_types():
    assert list_to_dict({'a': 1, 'b': 2}, 'word') == 'First arg must be list!'
    assert list_to_dict((2, 3, 4), 2) == 'First arg must be list!'


def test_list_to_dict_ok():
    assert list_to_dict([], '172') == dict()
    assert list_to_dict([2, 3, 4, 5, 6, 2], 2) == {
        2: (5, True, 5),
        3: (1, False, 5),
        4: (2, False, 5),
        5: (3, False, 5),
        6: (4, False, 5),
    }
    assert list_to_dict(['aa', 2, 'test', 3, True, '2'], 2) == {
        'aa': (0, False, 6),
        2: (1, True, 6),
        'test': (2, False, 6),
        3: (3, False, 6),
        True: (4, False, 6),
        '2': (5, False, 6)
    }
    assert list_to_dict([1, 2, 4, 3, 6, 2, 4, 1], 3) == {
        1: (7, False, 5),
        2: (5, False, 5),
        3: (3, True, 5),
        4: (6, False, 5),
        6: (4, False, 5),
    }
    assert list_to_dict(['a', 'b', 'c'], 1) == {
        'a': (0, False, 3),
        'b': (1, False, 3),
        'c': (2, False, 3),
    }
