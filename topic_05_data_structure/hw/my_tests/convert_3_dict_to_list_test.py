"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""

from topic_05_data_structure.hw.convert_3_dict_to_list import dict_to_list


def test_dict_to_list_wrong_types():
    assert dict_to_list([1, 2, 3, 4]) == 'Must be dict!'
    assert dict_to_list('test_string') == 'Must be dict!'
    assert dict_to_list(3.14) == 'Must be dict!'


def test_dict_to_list_empty_dict():
    assert dict_to_list({}) == ([], [], 0, 0)
    assert dict_to_list(dict()) == ([], [], 0, 0)


def test_dict_to_list_ok():
    assert dict_to_list({'a': 2, 'b': 16, 'c': 183}) == (['a', 'b', 'c'], [2, 16, 183], 3, 3)
    assert dict_to_list({'lde': 1, 7: 1, 'hello_world': 'test'}) == (
        ['lde', 7, 'hello_world'], [1, 1, 'test'], 3, 2
    )
    assert dict_to_list({'a': 1, 'b': 1, 'c': 1, 'd': '1'}) == (
        ['a', 'b', 'c', 'd'], [1,1,1,'1'], 4, 2
    )
