from topic_05_data_structure.hw.list_1_count_word import count_word


def test_count_word_ok():
    assert count_word(['aa', 'vvv', 'dhwif', 'cc', 'aa'], 'aa') == 2


def test_count_word_different_types():
    assert count_word(['aa', 1, True, 'cc', 'tt'], 'aa') == 1


def test_count_word_zeros():
    assert count_word(['aa', 1, True, 'cc', 'tt'], 'a') == 0
    assert count_word(['aa', 1, True, 'cc', 'aa'], 'eee') == 0
    assert count_word(['l', 'i', 's', 't'], '') == 0


def test_count_word_list_wrong():
    assert count_word('test_string', 'word') == 'First arg must be list!'
    assert count_word(5, 'word') == 'First arg must be list!'
    assert count_word({}, 'word') == 'First arg must be list!'


def test_count_word_str_wrong():
    assert count_word(['a', 'b', 'c'], 5) == 'Second arg must be str!'
    assert count_word(['d', 'e', 'f'], ['l', 'i', 's', 't']) == 'Second arg must be str!'
    assert count_word(['a', 'b', 'c'], False) == 'Second arg must be str!'


def test_count_word_len_zero():
    assert count_word([], 'word') == 'Empty list!'
    assert count_word(list(), 'word') == 'Empty list!'
