from topic_05_data_structure.hw.convert_2_list_to_str import list_to_str


def test_list_to_str_wrong_types():
    assert list_to_str({'a': 1, 'b': 2}, 'word') == 'First arg must be list!'
    assert list_to_str((2, 3, 4), 'word') == 'First arg must be list!'
    assert list_to_str([1, 2, 3], 2) == 'Second arg must be str!'
    assert list_to_str(['a', 'b', 'c'], [2, 6, 7]) == 'Second arg must be str!'


def test_list_to_str_ok():
    assert list_to_str([], '*!*') == tuple()
    assert list_to_str([2, 3, 4, 5, 6, 2], '~') == ('2~3~4~5~6~2', 25)
    assert list_to_str(['1'], '!') == ('1', 0)
    assert list_to_str(['regs', 'aasf', [2, 5.3, 0], 728, True], '*') == (
        'regs*aasf*[2, 5.3, 0]*728*True', 16
    )
    assert list_to_str(['a', 'c', '*', 'm'], '**') == ('a**c*****m', 9)
