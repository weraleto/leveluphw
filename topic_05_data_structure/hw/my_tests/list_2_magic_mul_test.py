from topic_05_data_structure.hw.list_2_magic_mul import magic_mul


def test_magic_mul_ok():
    assert magic_mul([1, 'aa', 99]) == [1, 1, 'aa', 99, 1, 'aa', 99, 1, 'aa', 99, 99]
    assert magic_mul(['test']) == ['test', 'test', 'test', 'test', 'test']


def test_magic_mul_wrong_type():
    assert magic_mul('Lorem ipsum dolor sit amet!') == 'Must be list!'
    assert magic_mul({'a': 1, 'b': 2}) == 'Must be list!'
    assert magic_mul(55) == 'Must be list!'


def test_magic_mul_len_zero():
    assert magic_mul([]) == 'Empty list!'
    assert magic_mul(list()) == 'Empty list!'
