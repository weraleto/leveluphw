from topic_05_data_structure.hw.list_3_magic_reversal import magic_reversal


def test_magic_reversal_wrong_type():
    assert magic_reversal('This is test string!') == 'Must be list!'
    assert magic_reversal(156) == 'Must be list!'
    assert magic_reversal(False) == 'Must be list!'


def test_magic_reversal_len_0():
    assert magic_reversal([]) == 'Empty list!'
    assert magic_reversal(list()) == 'Empty list!'


def test_magic_reversal_ok():
    assert magic_reversal(['1', 2, '8', True]) == ['8', 2, True, '8', 2, '1']
    assert magic_reversal([1, 'aa', 99]) == ['aa', 'aa', 99, 'aa', 1]


def test_magic_reversal_list_not_changed():
    test_list = ['1', 2, '8', True]
    magic_reversal(test_list)
    assert test_list == ['1', 2, '8', True]


def test_magic_reversal_len_2():
    test_list = ['aa', 'bb']
    assert magic_reversal(test_list) == ['aa', 'bb', 'bb', 'aa']


def test_magic_reversal_len_1():
    test_list = ['aa']
    assert magic_reversal(test_list) == ['aa', 'aa', 'aa']
