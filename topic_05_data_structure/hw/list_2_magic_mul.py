"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list: list):
    if type(my_list) is not list:
        return 'Must be list!'
    if not len(my_list):
        return 'Empty list!'
    result = my_list * 3
    result.append(my_list[-1])
    result.insert(0, my_list[0])
    return result


if __name__ == '__main__':
    print(magic_mul(['aa']))
