"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(li: list, sep: str):
    if type(li) is not list:
        return 'First arg must be list!'
    elif type(sep) is not str:
        return 'Second arg must be str!'
    elif not li:
        return tuple()
    new_str = sep.join(str(x) for x in li)
    return new_str, new_str.count(sep) ** 2


if __name__ == '__main__':
    print(list_to_str([1, '2', 'awe', [1, 2, 3]], '!'))
