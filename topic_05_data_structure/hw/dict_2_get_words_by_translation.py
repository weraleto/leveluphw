"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(dictionary: dict, word: str):
    if type(dictionary) is not dict:
        return "Dictionary must be dict!"
    elif type(word) is not str:
        return "Word must be str!"
    elif len(dictionary) == 0:
        return "Dictionary is empty!"
    elif len(word) == 0:
        return "Word is empty!"
    names = list()
    for key, values in dictionary.items():
        if word in values:
            names.append(key)
    return names if len(names) else f"Can't find English word: {word}"
