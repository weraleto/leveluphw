"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""

from itertools import zip_longest


def zip_car_year(var1: list, var2: list):
    if type(var1) is not list or type(var2) is not list:
        return 'Must be list!'
    elif not len(var1) or not len(var2):
        return 'Empty list!'
    return list(zip_longest(var1, var2, fillvalue='???'))
