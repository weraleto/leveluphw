from topic_07_oop.hw.class_2_movie import Movie


def test_movie_ok():
    duration_min, name, year = 90, 'Frozen', 2015
    new_movie = Movie(duration_min, name, year)
    assert new_movie.duration_min == duration_min
    assert new_movie.name == name
    assert new_movie.year == year

    assert str(new_movie) == f"Наименование фильма: {name} | Год выпуска: {year} | Длительность (мин): {duration_min}"


def test_movies_equality():
    movie1 = Movie(154, 'Pulp Fiction', 1994)
    movie2 = Movie(90, 'Frozen', 2015)
    movie3 = Movie(120, 'Requiem for a Dream', 2000)
    movie4 = Movie(154, 'My Movie', 2020)

    assert (movie1 >= movie2) is True
    assert (movie2 >= movie3) is False
    assert (movie1 >= movie3) is True
    assert (movie1 >= movie4) is True
