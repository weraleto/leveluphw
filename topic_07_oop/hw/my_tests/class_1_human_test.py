from topic_07_oop.hw.class_1_human import Human


def test_new_human():
    age, first_name, last_name = 22, 'Oleg', 'Petrov'
    new_human = Human(age, first_name, last_name)

    assert new_human.age == age
    assert new_human.first_name == first_name
    assert new_human.last_name == last_name
    assert new_human.get_age() == age
    assert str(new_human) == f"Имя: {first_name} {last_name} Возраст: {age}"


def test_human_equality():
    human1 = Human(18, 'Vasia', 'Pupkin')
    human2 = Human(22, 'Ivan', 'Sidorov')
    human3 = Human(18, 'Vasia', 'Pupkin')

    assert (human1 == human2) is False
    assert (human2 == human3) is False
    assert (human1 == human3) is True
