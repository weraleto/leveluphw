from topic_07_oop.hw.class_4_2_worker import Worker


def test():
    name, salary, position = 'Oleg', 15000, 'chief'
    worker1 = Worker(name, salary, position)

    assert worker1.name == name
    assert worker1.salary == salary
    assert worker1.position == position

    assert len(worker1) == 5
    worker1.position = 'teacher'
    assert len(worker1) == 7


def test_workers_comparsion():
    worker1 = Worker('Oleg', 55000, 'policeman')
    worker2 = Worker('Ivan', 25000, 'detective')
    worker3 = Worker('Ivan', 20000, 'teacher')

    assert (worker1 > worker2) is True
    assert (worker2 > worker3) is True
    assert (worker3 > worker1) is False
