"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе +
get_pupil_count: вернуть сколько всего учеников в школе +
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker
from topic_07_oop.hw.class_4_3_school import School


def test():
    number = 105
    people = [
        Worker('John', 5000, 'teacher'),
        Worker('Michael', 7000, 'teacher'),
        Worker('Anna', 6000, 'teacher'),
        Worker('Emma', 8000, 'psychologist'),
        Worker('Tom', 7500, 'builder'),
        Worker('Rita', 5000, 'cooker'),
        Pupil('Sam', 10, {'math': [3, 5, 3, 2, 4], 'english': [4, 4, 4, 4, 3]}),
        Pupil('Polly', 12, {'math': [5, 4, 5, 4, 5], 'english': [4, 5, 4, 4, 4]}),
        Pupil('Rupert', 14, {'math': [5, 3, 3, 3, 3, 3], 'english': [4, 3, 3, 3, 3, 4]}),
    ]

    school = School(people, number)

    assert school.number == number

    t1 = [3, 5, 3, 2, 4, 4, 4, 4, 4, 3, 5, 4, 5, 4, 5, 4, 5, 4, 4, 4, 5, 3, 3, 3, 3, 3, 4, 3, 3, 3, 3, 4]

    assert round(school.get_avg_mark(), 2) == round(3.777777777777778, 2)
    assert round(school.get_avg_salary(), 2) == 6416.67
    assert school.get_worker_count() == 6
    assert school.get_pupil_count() == 3
    assert sorted(school.get_unique_worker_positions()) == sorted(['teacher', 'psychologist', 'builder', 'cooker'])
    assert school.get_max_pupil_age() == 14
    assert school.get_min_worker_salary() == 5000
    assert school.get_min_salary_worker_names() == ['John', 'Rita']
