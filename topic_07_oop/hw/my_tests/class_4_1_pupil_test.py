import pytest
from topic_07_oop.hw.class_4_1_pupil import Pupil


def test():
    name, age = 'Petya', 12
    marks = {
        'english': [4, 5, 3, 4, 5, 3],
        'math': [4, 4, 4, 5, 3, 5, 2],
        'it': [5, 5, 5],
        'arts': []
    }
    pupil_1 = Pupil(name, age, marks)
    assert pupil_1.name == name
    assert pupil_1.age == age
    assert sorted(pupil_1.get_all_marks()) == sorted([4, 5, 3, 4, 5, 3, 4, 4, 4, 5, 3, 5, 5, 5, 5, 2])

    assert pupil_1.get_avg_mark_by_subject('it') == pytest.approx(5.0, 0.1)
    assert pupil_1.get_avg_mark_by_subject('english') == pytest.approx(4.0, 0.1)
    assert pupil_1.get_avg_mark_by_subject('math') == pytest.approx(3.86, 0.1)
    assert pupil_1.get_avg_mark_by_subject('history') == 0

    assert pupil_1.get_avg_mark() == pytest.approx(4.13, 0.1)


def test_empty_marks():
    pupil_vasya = Pupil('Vasya', 14, {'math': [], 'english': []})
    assert pupil_vasya.get_avg_mark() == 0


def test_pupil_comparsion():
    pupil1 = Pupil(
        15, 'Oleg',
        {
            'english': [4, 5, 3, 4, 5, 3],
            'math': [4, 4, 4, 5, 3, 5, 2],
            'it': [5, 5, 5],
            'arts': []
        }
    )  # 3.89

    pupil2 = Pupil(
        16, 'Timofey',
        {
            'english': [5, 5, 5, 4, 5, 5],
            'math': [3, 4, 5, 3, 4],
            'it': [3, 4, 3, 4],
            'arts': [5]
        }
    )  # 4.19

    pupil3 = Pupil(
        14, 'Timofey',
        {
            'english': [5, 5, 5, 4, 5, 5],
            'math': [5, 4, 5, 5, 4],
            'it': [5, 5, 4],
            'arts': [4]
        }
    )  # 4.67
    assert (pupil1 <= pupil2) is True
    assert (pupil2 <= pupil3) is True
    assert (pupil3 <= pupil1) is False
