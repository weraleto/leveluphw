"""
Класс Chicken.

Поля:
имя: name,
номер загона: corral_num,
сколько яиц в день: eggs_per_day.

Методы:
get_sound: вернуть строку 'Ко-ко-ко',
get_info: вернуть строку вида:
"Имя курицы: name
 Номер загона: corral_num
 Количество яиц в день: eggs_per_day"
__lt__: вернуть результат сравнения количества яиц (<)
"""
from topic_07_oop.hw.class_3_1_chicken import Chicken


def test():
    name, corral_num, eggs_per_day = 'Kurochka', 2, 4
    chicken1 = Chicken(name, corral_num, eggs_per_day)
    chicken2 = Chicken('Zvezdochka', 6, 2)
    chicken3 = Chicken('Nesushka', 3, 5)

    assert chicken1.name == name
    assert chicken1.corral_num == corral_num
    assert chicken1.eggs_per_day == eggs_per_day

    assert chicken1.get_sound() == 'Ко-ко-ко'
    assert chicken1.get_info() == f"Имя курицы: {name}\n" \
                                  f"Номер загона: {corral_num}\n" \
                                  f"Количество яиц в день: {eggs_per_day}"

    assert (chicken1 < chicken2) is False
    assert (chicken2 < chicken3) is True
    assert (chicken1 < chicken3) is True
