"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


from topic_07_oop.hw.class_3_2_goat import Goat


def test():
    name, age, milk_per_day = 'Kozochka', 2, 4
    goat_1 = Goat(name, age, milk_per_day)

    assert goat_1.name == name
    assert goat_1.age == age
    assert goat_1.milk_per_day == milk_per_day

    assert goat_1.get_sound() == 'Бе-бе-бе'

    assert (goat_1 * 5).milk_per_day == 20
    assert (~goat_1).name == 'Akhcozok'

