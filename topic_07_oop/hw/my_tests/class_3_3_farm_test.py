from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat
from topic_07_oop.hw.class_3_3_farm import Farm


def test_farm():
    owner, name = 'Wera', 'My Farm'
    animals = [
        Chicken('Kura', 2, 3),
        Chicken('Raisa', 6, 1),
        Chicken('Ryaba', 1, 2),
        Goat('Kozochka', 3, 2),
        Goat('Vasilisa', 4, 4),
    ]
    new_farm = Farm(name, owner)

    assert new_farm.name == name
    assert new_farm.owner == owner
    assert len(new_farm.zoo_animals) == 0

    new_farm.zoo_animals.extend(animals)

    assert new_farm.get_chicken_count() == 3
    assert new_farm.get_goat_count() == 2
    assert new_farm.get_animals_count() == 5
    assert new_farm.get_eggs_count() == 6
    assert new_farm.get_milk_count() == 6


def test_no_chicken():
    owner, name = 'Wera', 'My Farm'
    animals = [
        Goat('Kozochka', 3, 2),
        Goat('Vasilisa', 4, 4),
    ]
    new_farm = Farm(name, owner)
    new_farm.zoo_animals.extend(animals)

    assert new_farm.get_chicken_count() == 0
    assert new_farm.get_goat_count() == 2
    assert new_farm.get_animals_count() == 2
    assert new_farm.get_eggs_count() == 0
    assert new_farm.get_milk_count() == 6
