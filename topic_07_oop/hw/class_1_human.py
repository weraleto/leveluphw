"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:
    def __init__(self, age: int, first_name: str, last_name: str):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def get_age(self):
        return self.age

    def __str__(self):
        return f"Имя: {self.first_name} {self.last_name} Возраст: {self.age}"

    def __eq__(self, other):
        is_equal_params = (
            self.age == other.age,
            self.first_name == other.first_name,
            self.last_name == other.last_name
        )
        return all(is_equal_params)


if __name__ == '__main__':
    h1 = Human(18, 'Vasia', 'Pupkin')
    h2 = Human(18, 'Vasia', 'Pupkin')
    # print(h1.get_age())
    # print(h1)
    print(h1 == h2)
