"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:
    def __init__(self, name: str, age: int, milk_per_day: int):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, num):
        self.milk_per_day *= int(num)
        return self

    # для отладки
    def __str__(self):
        return f"{self.name}, {self.age}, {self.milk_per_day}"


if __name__ == '__main__':
    goat_1 = Goat('Kozochka', 1, 5)
    # print(goat_1)
    # ~goat_1
    # goat_1 * 5
    # print(goat_1)
